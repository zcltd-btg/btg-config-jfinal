# btg-config-jfinal

#### 使用说明
```
String url = "jdbc:mysql://127.0.0.1:3306/test?characterEncoding=utf8&zeroDateTimeBehavior=convertToNull";
String username = "root";
String password = "root";
BtgSetPlugin.initBtgSetPlugin(url, username, password).setTemplet("demo.xml").init();

获取配置
BtgSet.getBoolean("xxx");
BtgSet.getString("xxx");
BtgSet.getInteger("xxx");

获取所有配置
JSONObject xxx = BtgSet.getParams();

访问插件配置页
http://xxx.xxx.xxx.xxx/xxx/btgset
```

#### 升级记录
```
4.0.5
1、注释init

#### 升级记录
```
4.0.4
1、添加自定义按钮,提示

4.0.3
1、添加自定义按钮；

v4.0.2
1、增加长度；

v4.0.1
1、转为独立maven依赖，去掉parent；

v3.0.3
1、btg-parent升级到v2.0.1；

v3.0.2
1、统一依赖管理；

v3.0.1
1、统一迁移至公司名下；

V1.0.6
1、新增首次进入插件时候需要进行用户登录
2、登录后在规定时间内无须再次登录

V1.0.5
1、修复当数据库存在表但无数据量初始化为空的错误

V1.0.3
1、当参数应用后提供回调方法,需实现接口ApplyCallBack,并调用BtgSetPlugin.setApplyCallBack

V1.0.2
1、btgset的xml配置中去掉名称的空格
2、btgset参数排序使用xml默认排序
3、修复btgset重置为默认配置时，需要手动应用配置才能生效的问题

V1.0.1
初始化
```
