package cn.zcltd.btg.config;

import cn.zcltd.btg.config.callback.ApplyCallBack;
import cn.zcltd.btg.config.callback.CustomAction;
import cn.zcltd.btg.config.http.BtgSetServlet;
import cn.zcltd.btg.config.util.BtgSetHelper;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Administrator on 2016/11/26.
 */
public class BtgSetPlugin {

    private String url;//jdbc连接地址
    private String username;//jdbc用户名
    private String password;//jdbc密码
    private String driverClass = null;//数据库驱动
    private DataSource dataSource = null;//连接池
    private Dialect dialect;//数据库方言
    private String setTemplet;//参数配置模板


    private int sessionTimeOut = 30;//sesssion失效时间
    private boolean isRefreshSession = false;//是否每次操作都刷新session

    protected List<ApplyCallBack> applyCallBackList = new ArrayList<ApplyCallBack>();

    public List<CustomAction> excuteCallBackList = new ArrayList<>();

    private Map<String, String> tabMap = new HashMap<String, String>();//表名对应关系

    private volatile static BtgSetPlugin btgSetPlugin;

    public BtgSetPlugin() {

    }

    public static BtgSetPlugin getBtgSetPlugin() {
        return btgSetPlugin;
    }

    public static BtgSetPlugin initBtgSetPlugin(DataSource dataSource, Dialect dialect) {
        if (btgSetPlugin == null) {
            synchronized (BtgSetPlugin.class) {
                if (btgSetPlugin == null)
                    btgSetPlugin = new BtgSetPlugin(dataSource, dialect);
            }
        }
        return btgSetPlugin;
    }

    public static BtgSetPlugin initBtgSetPlugin(String url, String username, String password) {
        if (btgSetPlugin == null) {
            synchronized (BtgSetPlugin.class) {
                if (btgSetPlugin == null)
                    btgSetPlugin = new BtgSetPlugin(url, username, password);
            }
        }
        return btgSetPlugin;
    }

    public static BtgSetPlugin initBtgSetPlugin(String url, String username, String password, String driverClass) {
        if (btgSetPlugin == null) {
            synchronized (BtgSetPlugin.class) {
                if (btgSetPlugin == null)
                    btgSetPlugin = new BtgSetPlugin(url, username, password, driverClass);
            }
        }
        return btgSetPlugin;
    }

    public static BtgSetPlugin initBtgSetPlugin(String url, String username, String password, Dialect dialect) {
        if (btgSetPlugin == null) {
            synchronized (BtgSetPlugin.class) {
                if (btgSetPlugin == null)
                    btgSetPlugin = new BtgSetPlugin(url, username, password, dialect);
            }
        }
        return btgSetPlugin;
    }

    public static BtgSetPlugin initBtgSetPlugin(String url, String username, String password, String driverClass, Dialect dialect) {
        if (btgSetPlugin == null) {
            synchronized (BtgSetPlugin.class) {
                if (btgSetPlugin == null)
                    btgSetPlugin = new BtgSetPlugin(url, username, password, driverClass, dialect);
            }
        }
        return btgSetPlugin;
    }

    private BtgSetPlugin(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
        initDialect(url);
    }

    private BtgSetPlugin(String url, String username, String password, String driverClass) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.driverClass = driverClass;
        initDialect(url);
    }

    private BtgSetPlugin(DataSource dataSource, Dialect dialect) {
        this.dataSource = dataSource;
        this.dialect = dialect;
    }

    private BtgSetPlugin(String url, String username, String password, Dialect dialect) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.dialect = dialect;
    }

    private BtgSetPlugin(String url, String username, String password, String driverClass, Dialect dialect) {
        this.url = url;
        this.username = username;
        this.password = password;
        this.driverClass = driverClass;
        this.dialect = dialect;
    }

    private void initDialect(String url) {
        if (url != null && !"".equals(url)) {
            if (url.toLowerCase().startsWith("jdbc:mysql")) {
                this.dialect = Dialect.MYSQL;
                return;
            }
            if (url.toLowerCase().startsWith("jdbc:oracle:thin")) {
                this.dialect = Dialect.ORACLE;
                return;
            }
            if (url.toLowerCase().startsWith("jdbc:sqlserver")) {
                this.dialect = Dialect.SQLSERVER;
                return;
            }
        }
    }

    public Dialect getDialect() {
        return this.dialect;
    }

    public BtgSetPlugin setTemplet(String path) {
        this.setTemplet = path;
        return this.btgSetPlugin;
    }

    public Map<String, String> getTabMap() {
        return this.tabMap;
    }

    public String getTemplet() {
        return this.setTemplet;
    }

    public BtgSetPlugin setApplyCallBack(ApplyCallBack callBack) {
        if (callBack != null)
            this.applyCallBackList.add(callBack);
        return this.btgSetPlugin;
    }



    public BtgSetPlugin setExcuteCallBack(CustomAction customAction) {
        if (customAction != null)
            this.excuteCallBackList.add(customAction);
        return this.btgSetPlugin;
    }

    public int getSessionTimeOut() {
        return this.sessionTimeOut;
    }

    public BtgSetPlugin setSessionTimeOut(int sessionTimeOut) {
        this.sessionTimeOut = sessionTimeOut;
        return this.btgSetPlugin;
    }

    public boolean getIsRefreshSession() {
        return this.isRefreshSession;
    }

    public BtgSetPlugin setIsRefreshSession(boolean isRefreshSession) {
        this.isRefreshSession = isRefreshSession;
        return this.btgSetPlugin;
    }

    //取得连接
    public Connection getConnection() {

        try {
            if (dataSource != null) {
                return dataSource.getConnection();
            } else {
                if (driverClass == null) {
                    switch (dialect) {
                        case MYSQL:
                            driverClass = "com.mysql.jdbc.Driver";
                            break;
                        case ORACLE:
                            driverClass = "oracle.jdbc.driver.OracleDriver";
                            break;
                        case SQLSERVER:
                            driverClass = "com.microsoft.jdbc.sqlserver.SQLServerDriver";
                            break;
                    }
                }
                Class.forName(driverClass);
                return DriverManager.getConnection(url, username, password);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void init() {
        init(null);
    }

    public void init(String tabSuffix) {
        if (tabSuffix == null)
            tabSuffix = "";
        List<String> list = BtgSetHelper.getInstance().getTabName(dialect);
        for (String str : list) {
            tabMap.put(str, tabSuffix + str);
        }

        Connection conn = null;
        Statement statement = null;
        ResultSet rs = null;

        try {
            conn = BtgSetPlugin.getBtgSetPlugin().getConnection();
            statement = conn.createStatement();
            String sql = "select count(1)row_no from " + tabMap.get("btg_set_cfg_module");
            rs = statement.executeQuery(sql);
            boolean con = false;
            while (rs.next()) {
                if (rs.getInt("row_no") > 0)
                    con = true;
            }
            //写入默认配置
            if (!con)
                BtgSetHelper.getInstance().writeDefaultSet();
        } catch (SQLException e) {
            //构造参数环境
            BtgSetHelper.getInstance().resetDataBaseEnv();
            //写入默认配置
            BtgSetHelper.getInstance().writeDefaultSet();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            BtgSetHelper.getInstance().initValue();
            try {
                if (statement != null)
                    statement.close();
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            BtgSetServlet.initBtgSetPlugin();
        }
    }


}
