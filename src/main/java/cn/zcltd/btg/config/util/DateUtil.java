package cn.zcltd.btg.config.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 工具类：时间处理
 */
public class DateUtil {
    protected static final Logger log = LoggerFactory.getLogger(DateUtil.class);

    public static final String DEFAULT_PATTERN_YEAR = "yyyy";
    public static final String DEFAULT_PATTERN_MONTH = "MM";
    public static final String DEFAULT_PATTERN_DAY = "dd";
    public static final String DEFAULT_PATTERN_HOUR = "HH";
    public static final String DEFAULT_PATTERN_MINUTE = "mm";
    public static final String DEFAULT_PATTERN_SECOND = "ss";
    public static final String DEFAULT_PATTERN_MILLISECOND = "SSS";
    public static final String DEFAULT_PATTERN_DATE = "yyyy-MM-dd";
    public static final String DEFAULT_PATTERN_TIME = "HH:mm:ss";
    public static final String DEFAULT_PATTERN_DATETIME = "yyyy-MM-dd HH:mm:ss";
    public static final String DEFAULT_PATTERN_TIMESTAMP = "yyyyMMddHHmmssSSS";

    /**
     * 获取当前系统时间
     *
     * @return 时间对象
     */
    public static Date getNow() {
        return new Date();
    }

    /**
     * 获取格式化日期或时间
     *
     * @param date          时间对象
     * @param formatPattern 格式化字符串
     * @return
     */
    public static String format(Date date, String formatPattern) {
        if (date == null) return "";
        if (formatPattern == null || "".equals(formatPattern)) formatPattern = DEFAULT_PATTERN_DATETIME;
        return new SimpleDateFormat(formatPattern).format(date);
    }

    /**
     * 使用参数Format将字符串转为Date
     *
     * @param strDate 日期字符串
     * @param pattern 格式化字符串
     * @return Date对象
     */
    public static Date parse(String strDate, String pattern) {
        if (strDate == null || "".equals(strDate)) return null;
        if (pattern == null || "".equals(pattern)) pattern = DEFAULT_PATTERN_DATETIME;
        try {
            return new SimpleDateFormat(pattern).parse(strDate);
        } catch (ParseException e) {
            log.error(DateUtil.class.getName(), e);
        }
        return null;
    }

    /**
     * 获取格式化年
     *
     * @param date 时间对象
     * @return
     */
    public static String formatYear(Date date) {
        return format(date, DEFAULT_PATTERN_YEAR);
    }

    /**
     * 获取格式化月
     *
     * @param date 时间对象
     * @return
     */
    public static String formatMonth(Date date) {
        return format(date, DEFAULT_PATTERN_MONTH);
    }

    /**
     * 获取格式化日
     *
     * @param date 时间对象
     * @return
     */
    public static String formatDay(Date date) {
        return format(date, DEFAULT_PATTERN_DAY);
    }

    /**
     * 获取格式化时
     *
     * @param date 时间对象
     * @return
     */
    public static String formatHour(Date date) {
        return format(date, DEFAULT_PATTERN_HOUR);
    }

    /**
     * 获取格式化分
     *
     * @param date 时间对象
     * @return
     */
    public static String formatMinute(Date date) {
        return format(date, DEFAULT_PATTERN_MINUTE);
    }

    /**
     * 获取格式化秒
     *
     * @param date 时间对象
     * @return
     */
    public static String formatSecond(Date date) {
        return format(date, DEFAULT_PATTERN_SECOND);
    }

    /**
     * 获取格式化毫秒
     *
     * @param date 时间对象
     * @return
     */
    public static String formatMillisecond(Date date) {
        return format(date, DEFAULT_PATTERN_MILLISECOND);
    }

    /**
     * 获取格式化日期
     *
     * @param date 时间对象
     * @return
     */
    public static String formatDate(Date date) {
        return format(date, DEFAULT_PATTERN_DATE);
    }

    /**
     * 获取格式化时间
     *
     * @param date 时间对象
     * @return
     */
    public static String formatTime(Date date) {
        return format(date, DEFAULT_PATTERN_TIME);
    }

    /**
     * 获取格式化日期和时间
     *
     * @param date 时间对象
     * @return
     */
    public static String formatDateTime(Date date) {
        return format(date, DEFAULT_PATTERN_DATETIME);
    }

    /**
     * 获取格式化时间戳
     *
     * @param date 时间对象
     * @return
     */
    public static String formatTimestamp(Date date) {
        return format(date, DEFAULT_PATTERN_TIMESTAMP);
    }

    /**
     * 获取当前系统日期
     *
     * @return
     */
    public static String getNowDate() {
        return formatDate(getNow());
    }

    /**
     * 获取当前系统时间
     *
     * @return
     */
    public static String getNowTime() {
        return formatTime(getNow());
    }

    /**
     * 获取当前系统日期和时间
     *
     * @return
     */
    public static String getNowDateTime() {
        return formatDateTime(getNow());
    }

    /**
     * 获取时间戳
     *
     * @return
     */
    public static String getNowTimestamp() {
        return formatTimestamp(getNow());
    }

    /**
     * 将日期字符串转化为Date对象(格式20010101123030222)
     *
     * @param strDate 日期字符串
     * @return Date对象
     */
    public static Date parseTimestamp(String strDate) {
        return parse(strDate, DEFAULT_PATTERN_TIMESTAMP);
    }
    /*
        =======================Calendar分割线==========================
     */

    /**
     * 将Date转换为Calendar
     *
     * @param date
     * @return
     */
    public static Calendar date2Calendar(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar;
    }

    /**
     * 日期计算
     *
     * @param date
     * @param field
     * @param amount
     * @return
     */
    public static Date dateAdd(Date date, int field, int amount) {
        Calendar calendar = date2Calendar(date);
        calendar.add(field, amount);
        return calendar.getTime();
    }
}