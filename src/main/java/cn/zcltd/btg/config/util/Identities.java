package cn.zcltd.btg.config.util;

import java.text.DecimalFormat;
import java.util.Random;
import java.util.UUID;

/**
 * 封装各种生成唯一性ID算法的工具类.
 */
public class Identities {

    /**
     * 获取一个指定区间[min,max]内的随机整数
     *
     * @param min
     * @param max
     * @return
     */
    public static String random(int min, int max) {
        return String.valueOf(new Random().nextInt(max - min + 1) + min);
    }

    /**
     * 获取一个指定长度的随机整数（支持0到9位长度）
     *
     * @param length
     * @return
     */
    public static String random(int length) {
        if (length <= 0) return "";
        length = length > 9 ? 9 : length;
        DecimalFormat format = new DecimalFormat("#");
        int min = Integer.valueOf(format.format(Math.pow(10, length - 1)));
        int max = Integer.valueOf(format.format(Math.pow(10, length))) - 1;
        return random(min, max);
    }

    /**
     * 获取一个原始的uuid（带中划线"-"）
     *
     * @return
     */
    public static String uuidOriginal() {
        return UUID.randomUUID().toString();
    }

    /**
     * 获取一个uuid
     *
     * @return
     */
    public static String uuid() {
        return uuidOriginal().replaceAll("-", "");
    }
}