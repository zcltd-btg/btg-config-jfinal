package cn.zcltd.btg.config.util;

import cn.zcltd.btg.config.BtgSetPlugin;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Administrator on 2017/1/23.
 */
public class _Utils {
    /**
     * 验证当前登录用户是否过期
     *
     * @param key
     * @return
     */
    public static boolean valid(String key) {
        if ("".equals(key) || key == null || key.indexOf("-") == -1)
            return false;

        String seed = key.substring(0, key.indexOf("-"));
        if (seed.length() < 10)
            return false;
        long times = 0l;
        try {
            times = Long.valueOf(seed);
        } catch (Exception e) {
            return false;
        }
        String re = getKey(times);

        if (!key.equals(re))
            return false;

        long curr = System.currentTimeMillis();

        if (curr < times)
            return false;

        long sec = curr - times;

        if (BtgSetPlugin.getBtgSetPlugin().getSessionTimeOut() * 60 * 1000 < sec)
            return false;

        return true;
    }

    public static String getKey(long times) {

        String key = Long.toString(times);
        int i = Integer.parseInt(key.substring(7));
        i = i << Integer.parseInt(key.substring(4, 5));
        try {
            key = calcMD5(Integer.toBinaryString(i)).toUpperCase();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return String.valueOf(times) + "-" + key;
    }

    public static String calcMD5(String md5Str) {

        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            byte[] bytes = md5.digest(md5Str.getBytes("UTF-8"));
            StringBuffer sb = new StringBuffer();
            String temp = "";
            for (byte b : bytes) {
                temp = Integer.toHexString(b & 0XFF);
                sb.append(temp.length() == 1 ? "0" + temp : temp);
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            return null;
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }
}
