package cn.zcltd.btg.config.callback;


import com.alibaba.fastjson.JSONObject;

/**
 * Created by Administrator on 2016/12/26.
 */
public interface ApplyCallBack {
    void callBack(JSONObject dirty, JSONObject now);
}
