package cn.zcltd.btg.config.http;

import com.alibaba.fastjson.JSONObject;
import cn.zcltd.btg.config.BtgSetPlugin;
import cn.zcltd.btg.config.util.Utils;
import cn.zcltd.btg.config.util._Utils;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Enumeration;

/**
 * Created by Administrator on 2016/12/1.
 */
public class ResourceServlet extends HttpServlet {
    final String keyName = "btgaccess";

    protected final String resourcePath;

    public ResourceServlet(String resourcePath) {
        this.resourcePath = resourcePath;
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String contextPath = req.getContextPath();
        String servletPath = req.getServletPath();
        String requestURI = req.getRequestURI();

        req.setCharacterEncoding("utf-8");
        resp.setCharacterEncoding("utf-8");

        if (contextPath == null) { // root context
            contextPath = "";
        }
        String uri = contextPath + servletPath;
        String path = requestURI.substring(contextPath.length() + servletPath.length());
        if ("".equals(path)) {
            if (contextPath.equals("") || contextPath.equals("/")) {
                resp.sendRedirect("/btgset/login.html");
            } else {
                resp.sendRedirect("btgset/login.html");
            }
            return;
        }

        if ("/".equals(path)) {
            resp.sendRedirect("login.html");
            return;
        }
        if (path.endsWith("/login.html")) {
            if (validLogin(req)) {
                resp.sendRedirect("index.html");
                return;
            }
        }
        if (path.contains(".json")) {
            if (!path.contains("/login.json") && !validLogin(req)) {//验证是否登录
                JSONObject json = new JSONObject();
                json.put("code", -1);
                json.put("msg", "SessionTimeOut");
                json.put("url", "login.html");
                resp.getWriter().print(json);
                return;
            }
            String fullUrl = path;
            if (req.getQueryString() != null && req.getQueryString().length() > 0) {
                fullUrl += "?" + req.getQueryString();
            }
            JSONObject obj = (JSONObject) process(fullUrl, getParas(req), req, resp);

            if (BtgSetPlugin.getBtgSetPlugin().getIsRefreshSession()) {
                obj.put(keyName, _Utils.getKey(System.currentTimeMillis()));
                obj.put("key", keyName);
            }
            resp.getWriter().print(obj);
            return;
        }
        returnResourceFile(path, uri, resp);
    }

    protected String getFilePath(String fileName) {
        return resourcePath + fileName;
    }

    protected void returnResourceFile(String fileName, String uri, HttpServletResponse response)
            throws ServletException,
            IOException {

        String filePath = getFilePath(fileName);

        if (filePath.endsWith(".html")) {
            response.setContentType("text/html; charset=utf-8");
        }
        if (fileName.endsWith(".jpg") || fileName.endsWith(".png") || fileName.endsWith(".gif")) {
            byte[] bytes = Utils.readByteArrayFromResource(filePath);
            if (bytes != null) {
                response.getOutputStream().write(bytes);
            }

            return;
        }

        String text = Utils.readFromResource(filePath);
        if (text == null) {
            response.sendRedirect(uri + "/login.html");
            return;
        }
        if (fileName.endsWith(".css")) {
            response.setContentType("text/css;charset=utf-8");
        } else if (fileName.endsWith(".js")) {
            response.setContentType("text/javascript;charset=utf-8");
        }
        response.getWriter().write(text);
    }

    private JSONObject getParas(HttpServletRequest req) {
        JSONObject object = new JSONObject();

        // 获取request参数
        Enumeration en = req.getParameterNames();
        while (en.hasMoreElements()) {
            String paramName = (String) en.nextElement();
            String paramValue = req.getParameter(paramName);
            object.put(paramName, paramValue);
        }
        return object;
    }

    private Object process(String url, JSONObject params, HttpServletRequest req, HttpServletResponse resp) {

        String methodName = url.substring(url.lastIndexOf("/") + 1, url.indexOf("."));
        try {

            // 利用反射获取方法
            Method method = getClass().getDeclaredMethod(methodName, JSONObject.class, HttpServletRequest.class, HttpServletResponse.class);
            // 执行相应的方法
            return method.invoke(this, params, req, resp);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new JSONObject();
    }

    /**
     * 验证登录是否过期
     *
     * @param req
     * @return
     */
    private boolean validLogin(HttpServletRequest req) {
        Cookie[] cookies = req.getCookies();
        if (cookies == null || cookies.length == 0)
            return false;
        String v = "";
        for (Cookie c : cookies) {
            if (keyName.equals(c.getName())) {
                v = c.getValue();
                break;
            }
        }
        if ("".equals(v))
            return false;

        return _Utils.valid(v);
    }

}
